INSERT INTO `products` (title, body, slug, quantity, price)
VALUES ('title1', 'body1', 'title-slug1', 20, 2851),
       ('title2', 'body2', 'title-slug2', 30, 68401),
       ('title3', 'body3', 'title-slug3', 70, 85123),
       ('title4', 'body4', 'title-slug4', 51, 571),
       ('title5', 'body5', 'title-slug5', 86, 1671);