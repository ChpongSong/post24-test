<?php session_start(); ?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title><?php getenv("APP_NAME", 'test') ?></title>
</head>

<header>
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/"><?php echo $_ENV['APP_NAME'] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/products/create">Create</a>
                    </li>
                    <li class="nav-item">
                        <div class="input-group input-group-sm mt-1 ml-5">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm">Search</span>
                            </div>
                            <input type="text" list="product-list" id="productSearch" class="form-control"
                                   aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                            <datalist id="product-list">
                            </datalist>
                        </div>
                    </li>
                </ul>
            </div>
            <?php if ($_SESSION['is_auth']): ?>
                <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Exit
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <btn id="userLogout" class="dropdown-item">logout</btn>
                            </div>
                        </li>
                    </ul>
                </div>
            <?php else: ?>
                <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/auth/login">Login</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="/auth/register">Register</a>
                        </li>
                    </ul>
                </div>
            <?php endif ?>
        </nav>
    </div>
</header>


<body>
<div class="container mt-4">
    <form enctype="multipart/form-data" method="POST" data-product-id="<?php echo $data['product']['id'] ?>" id="productEditForm">
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" value="<?php echo $data['product']['title'] ?>" class="form-control"
                   aria-describedby="emailHelp">
        </div>
        <div class="form-group">
            <label>Quantity</label>
            <input type="number" min="1" name="quantity" value="<?php echo $data['product']['quantity'] ?>" class="form-control">
        </div>
        <div class="form-group">
            <label>Price</label>
            <input type="number" min="1" name="price" value="<?php echo $data['product']['price'] ?>" class="form-control">
        </div>
        <div class="form-group">
            <label>Categories multiple</label>
            <select multiple class="form-control" name="categories[]">
                <?php foreach ($data['allCategories'] as $category): ?>
                    <?php foreach ($data['categories'] as $cat): ?>
                        <?php if ($category['id'] === $cat['id']): ?>
                            <option value="<?php echo $category['id'] ?>"
                                    selected><?php echo $category['name'] ?></option>
                        <?php else: ?>
                            <option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
                        <?php endif ?>
                    <?php endforeach; ?>
                <?php endforeach ?>
            </select>
        </div>
        <div class="form-group">
            <label>Body</label>
            <textarea name="body" class="form-control"
                      rows="3">
                <?php echo $data['product']['body'] ?>
            </textarea>
        </div>
        <button type="button" id="productEditBtn"class="btn btn-primary">Create</button>
    </form>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios@0.20.0-0/dist/axios.min.js"></script>
<script src="http://post24.loc/app.js"></script>
<script>
    const productEditForm = document.getElementById("productEditForm");
    const productEditBtn = document.getElementById("productEditBtn");

    productEditBtn.addEventListener("click", function (event) {
        event.preventDefault();
        let formData = {};
        for (let input of productEditForm.getElementsByTagName("input")) {
            formData[input.name] =  input.value;
        }
        let body = productEditForm.getElementsByTagName("textarea")[0];
        formData['body'] = body.value;
        let selectedOptions = productEditForm.getElementsByTagName("select")[0].selectedOptions;
        let categories = [];
        for (let category of selectedOptions) {
            categories.push(category.value);
        }
        formData['categories'] = categories;
        const productId = productEditForm.dataset.productId;
        const res = axios.post(`${window.location.origin}/products/update/${productId}`, {
            data: {
                input: formData
            }
        }).then(state => {
            if (state.status === 201) {
                window.location.replace(`${window.location.origin}`)
            }
        })
    })
</script>
</html>