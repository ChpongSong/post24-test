<?php session_start(); ?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title><?php echo getenv("APP_NAME", 'test') ?></title>
</head>

<header>
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/"><?php echo $_ENV['APP_NAME'] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/products/create">Create</a>
                    </li>
                    <li class="nav-item">
                        <div class="input-group input-group-sm mt-1 ml-5">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm">Search</span>
                            </div>
                            <input type="text" list="product-list" id="productSearch" class="form-control"
                                   aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                            <datalist id="product-list">
                            </datalist>
                        </div>
                    </li>
                </ul>
            </div>
            <?php if ($_SESSION['is_auth']): ?>
                <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Exit
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <btn id="userLogout" class="dropdown-item">logout</btn>
                            </div>
                        </li>
                    </ul>
                </div>
            <?php else: ?>
                <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/auth/login">Login</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="/auth/register">Register</a>
                        </li>
                    </ul>
                </div>
            <?php endif ?>
        </nav>
    </div>
</header>


<body>
<div class="container mt-4">
    <div class="card mb-3">
        <img class="card-img-top" src="https://png.pngtree.com/png-clipart/20190903/original/pngtree-computer-icon-png-image_4422771.jpg" alt="Card image cap">
        <div class="card-body">
            <h2 class="card-title">Title: <?php echo $data["product"]['title']?>
                <a href="../edit/<?php echo $data['product']['id'] ?>" class="btn btn-secondary">Edit</a>
                <button id="removeProductBtn" data-prod-id="<?php echo $data['product']['id'] ?>" class="btn btn-danger">Remove</button>
            </h2>
            <h2 class="card-title">Categories:
                <div class="row">
                    <?php foreach ($data['categories'] as $category):?>
                         <div class="col-sm-3 bg-secondary rounded m-2"><?php echo $category['name'] ?></div>
                    <?php endforeach?>
                </div>
            </h2>
            <p class="card-text"><h5>Body: <?php echo $data["product"]['body']?></h5></p>
            <p class="card-text"><small class="text-muted"><?php echo $data["product"]['updated_at']?></small></p>
        </div>
    </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios@0.20.0-0/dist/axios.min.js"></script>
<script src="http://post24.loc/app.js"></script>
<script>
    const removeProductBtn = document.getElementById("removeProductBtn");
    removeProductBtn.addEventListener("click", async function (event) {
        const prodId = removeProductBtn.dataset.prodId;
        // http://post24.loc/products/delete/1
        const url = `${window.location.origin}/products/delete/${prodId}`
        // console.log(url)
        const res = await window.axios.post(url)
        if (res.status === 204) {
            window.location.replace(window.location.origin);
        }
    })
</script>
</html>