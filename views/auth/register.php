<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title><?php echo getenv("APP_NAME", 'test') ?></title>
</head>
<header>
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/"><?php echo getenv("APP_NAME", 'test') ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <div class="input-group input-group-sm mt-1 ml-5">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm">Search</span>
                            </div>
                            <input type="text" list="product-list" id="productSearch" class="form-control"
                                   aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                            <datalist id="product-list">
                            </datalist>
                        </div>
                    </li>
                </ul>
            </div>
            <?php if ($_SESSION['is_auth']): ?>
                <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Exit
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <btn id="userLogout" class="dropdown-item">logout</btn>
                            </div>
                        </li>
                    </ul>
                </div>
            <?php else: ?>
                <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/auth/login">Login</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="/auth/register">Register</a>
                        </li>
                    </ul>
                </div>
            <?php endif ?>
        </nav>
    </div>
</header>
<body>
<div class="container mt-4">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <form METHOD="POST" id="registerForm">
                <div class="form-group">
                    <label>firstName</label>
                    <input type="text" class="form-control" name="first_name">
                </div>
                <div class="form-group">
                    <label>lastName</label>
                    <input type="text" class="form-control" name="last_name">
                </div>
                <div class="form-group">
                    <label>secondName</label>
                    <input type="text" class="form-control" name="second_name">
                </div>
                <div class="form-group">
                    <label>Email address</label>
                    <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                <button type="button" id="registerBtn" class="btn btn-primary">Register</button>
            </form>
        </div>
    </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios@0.20.0-0/dist/axios.min.js"></script>
<script>
    const registerForm = document.getElementById("registerForm");
    const registerBtn = document.getElementById("registerBtn");

    registerBtn.addEventListener("click", function (event) {
        event.preventDefault();

        let formData = {};
        for (let input of registerForm.getElementsByTagName("input")) {
            formData[input.name] =  input.value;
        }

        console.log(formData);

        const res = axios.post(`${window.location.origin}/auth/signUp`, {
            data: {
                input: formData
            }
        }).then(res => {
            if (res.status === 201) {
                window.location.replace(window.location.origin);
            }
        })
    })
</script>
</html>