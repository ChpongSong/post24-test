<?php


namespace App\Contracts;


interface RelationTypes
{
    const MANY_TO_MANY = "many-to-many";
    const ONE_TO_MANY = "one-to-many";
    const ONE_TO_ONE = "one-to-one";
}