<?php

namespace App\Facades;

class Route
{
    const CTL_NAMESPACE = 'App\\Http\\Controllers\\';

    public static function buildRoute()
    {
        $routes = require_once __DIR__ . "/../../routes/web.php";
        $regExAlias = require_once __DIR__ . "/../../routes/regex-alias.php";
        $find = array_keys($regExAlias);
        $replace = array_values($regExAlias);
        $basePath = getenv("APP_URL", "localhost");

        foreach ($routes as $routeName => $route) {
            $prefix = $route["prefix"];
            if (preg_match('/.+-group/u', $routeName)) {
                $namespace = self::CTL_NAMESPACE . $route["namespace"];
                foreach ($route["endpoints"] as $url => $endpoint) {
                    if (strpos($url, ':') !== false) {
                        $groupUrl = '/' . $prefix . str_replace($find, $replace, $url);
                    }

                    $explode = explode('@', $endpoint);
                    $ctlName = $explode[0];
                    $explode = explode('#', $explode[1]);
                    $methodName = $explode[0];
                    $urlMethod = $explode[1];
                    if ($groupUrl !== null) {
                        if (preg_match('#^' . $groupUrl . '$#', $_SERVER["REQUEST_URI"], $params)
                            && $urlMethod === $_SERVER["REQUEST_METHOD"]
                        ) {
                            $className = $namespace . '\\' . $ctlName;
                            $ctl = new $className;
                            call_user_func_array([$ctl, $methodName], [$params]);
                        }
                    }

                    else {
                        if ('/' . $prefix . '/'. $url === $_SERVER["REQUEST_URI"]
                            && $urlMethod === $_SERVER["REQUEST_METHOD"]
                        ) {
                            $className = $namespace . '\\' . $ctlName;
                            $ctl = new $className;
                            call_user_func([$ctl, $methodName]);
                        } else {
                            http_response_code(404);
                        }
                    }
                }

            } else {
                if (strpos($routeName, ':') !== false) {
                    $rName = str_replace($find, $replace, $routeName);
                }
                if (preg_match('#^' . $rName . '$#', $_SERVER["REQUEST_URI"], $params)) {
                    $explode = explode('@', $route);
                    $ctlName = $explode[0];
                    $explode = explode("#", $explode[1]);
                    $methodName = $explode[0];
                    $urlMethod = $explode[1];
                    if ($urlMethod === $_SERVER["REQUEST_METHOD"]) {
                        $className = self::CTL_NAMESPACE . $ctlName;
                        $ctl = new $className;
                        call_user_func_array([$ctl, $methodName], [$params]);
                    } else {
                        http_response_code(405);
                    }
                }
                elseif ($routeName === $_SERVER["REQUEST_URI"]){
                    $explode = explode('@', $route);
                    $ctlName = $explode[0];
                    $explode = explode("#", $explode[1]);
                    $methodName = $explode[0];
                    $urlMethod = $explode[1];
                    if ($urlMethod === $_SERVER["REQUEST_METHOD"]) {
                        $className = self::CTL_NAMESPACE . $ctlName;
                        $ctl = new $className;
                        call_user_func([$ctl, $methodName]);
                    } else {
                        http_response_code(405);
                    }
                }
            }
        }
    }
}