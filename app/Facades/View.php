<?php


namespace App\Facades;

class View
{

    public static function render(string $template, array $data = [])
    {
        $explode = explode(".", $template);
        $tplPath = getenv("ROOT_DIR") . '/views/';
        foreach ($explode as $item) {
             $tplPath .= $item . '/';
        }
        $tplPath = substr($tplPath, 0, -1);
        include $tplPath .='.php';
    }
}