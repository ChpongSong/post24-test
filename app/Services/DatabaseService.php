<?php

namespace App\Services;

use App\Contracts\Service;
use PDO;

class DatabaseService implements Service
{
    private $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ];

    protected $connection;

    public function openConnection()
    {
        try {
            $dsn = getenv("DB_CONNECTION") . ":host=" .
                getenv("DB_HOST") . ";dbname=" .
                getenv("DB_NAME") . ";charset=" . getenv("DB_CHARSET");
            $this->connection = new PDO(
                $dsn,
                getenv("DB_USERNAME"),
                getenv("DB_PASSWORD"),
                $this->options
            );
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $this->connection;
        } catch (\PDOException $e)
        {
            file_put_contents(getenv("ROOT_DIR")."/pdoerror.log","There is some problem in connection: " . $e->getMessage());
        }
    }

    public function closeConnection()
    {
        $this->connection = null;
    }
}