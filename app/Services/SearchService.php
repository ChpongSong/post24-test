<?php


namespace App\Services;


use App\Contracts\Service;
use App\Models\Product;

class SearchService implements Service
{
    /**
     * @return array
     */
    public function getPopularProducts(): array
    {
        $result = new Product();
        $db = $result->databaseService->openConnection();
        try {
            return $db->query("SELECT `id`,`title` FROM {$result->getTable()} LIMIT 5")->fetchAll();
        } catch (\PDOException $exception) {
            dump($exception);
            return $exception->getMessage();
        }
    }

    public function getProductByName(string $productName)
    {
        $result = new Product();
        $db = $result->databaseService->openConnection();
        try {
            $test = $db->query("SELECT * FROM {$result->getTable()}
                WHERE title LIKE '%{$productName}%'");
            return $test;
        } catch (\PDOException $exception) {
            print_r($exception);
            return $exception->getMessage();
        }
    }
}