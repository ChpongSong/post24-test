<?php


namespace App\Http\Controllers\API;


use App\Contracts\Service;
use App\Http\Controllers\Controller;
use App\Services\SearchService;

class SearchController extends Controller
{
    private Service $searchService;

    public function __construct()
    {
        $this->searchService = new SearchService();
    }

    /**
     * @return array
     */
    public function getPopularProducts(): array
    {
        header('Content-Type: application/json');
        echo $this->arrayToJson($this->searchService->getPopularProducts());
        header("HTTP/1.0 200");
        return [];
    }

    /**
     * @return array
     */
    public function getProductByName(): array
    {
        header("Content-Type: application/json");
        $body = file_get_contents('php://input');
        $request = json_decode($body, true);

        if (isset($request)) {
            header("HTTP/1.0 200");
            echo $this->arrayToJson($this->searchService->getProductByName($request['data']['productName'])->fetchAll());
        }
        return [];
    }
}