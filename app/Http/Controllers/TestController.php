<?php


namespace App\Http\Controllers;


class TestController extends Controller
{
    public function split()
    {
        dump('split');
    }

    public function index()
    {
        dump("index");
    }

    public function show($params)
    {
        dd($params);
    }
}