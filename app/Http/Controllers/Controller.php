<?php


namespace App\Http\Controllers;


abstract class Controller
{
    public function arrayToJson($array)
    {
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}