<?php

namespace App\Http\Controllers\Auth;

use App\Facades\View;
use App\Helpers\SqlBuilder;
use App\Http\Controllers\Controller;
use App\Models\User;

session_start();

class AuthController extends Controller
{
    public function login()
    {
        header("HTTP/1.0 200");
        View::render("auth.login");
    }

    public function signIn()
    {
        $user = new User();
        header("Content-Type: application/json");
        $request = json_decode(file_get_contents('php://input'), true);
        if ($authCredentials = $user->getByCredentials($request['data']['input'])) {
            $_SESSION['auth'] = $authCredentials;
            $_SESSION['is_auth'] = true;
            header("HTTP/1.0 200");
            echo json_encode([
                "status" => $_SESSION['auth'],
                "time" => time()
            ], JSON_UNESCAPED_UNICODE);
        }
        else {
            header("HTTP/1.0 400");
            echo json_encode([
                "errors" => [
                    "user not found"
                ],
                "time" => time()
            ], JSON_UNESCAPED_UNICODE);
        }
    }

    public function logout()
    {
        header("HTTP/1.0 200");
        session_destroy();
        $_SESSION['auth'] = [];
        $_SESSION['is_auth'] = false;
    }

    public function register()
    {
        header("HTTP/1.0 200");
        View::render("auth.register");
    }

    public function signUp()
    {
        header("Content-Type: application/json");
        $request = json_decode(file_get_contents('php://input'), true);
        $input = $request['data']['input'];
        $input["password"] = md5($input["password"]);
        $user = new User();
        $db = $user->databaseService->openConnection();
        $query = SqlBuilder::generateInsertSql('users', $input);
        $db->prepare($query)->execute($input);
        header("HTTP/1.0 201");
    }
}