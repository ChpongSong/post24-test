<?php

namespace App\Http\Controllers\Products;

use App\Facades\View;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        $products = new Product();
        $products = $products->getALl();
        foreach ($products as $product) {
            $product["categories"] = (new Product())->categories($product["id"]);
            $array[] = $product;
        }
        header("HTTP/1.0 200");
//        header('Content-Type: application/json');
//        echo json_encode($array, JSON_UNESCAPED_UNICODE);
        View::render("products.index", [
            "products" => $array
        ]);
    }

    public function show($params)
    {
        $product = (new Product())->find($params['id']);
        header("HTTP/1.0 200");

        View::render("products.show", [
            "product" => $product,
            "categories" => (new Product())->categories($product['id'])
        ]);
    }

    public function create()
    {
        $categories = (new Category())->getAll();
        header("HTTP/1.0 200");
        View::render("products.create", [
            "categories" => $categories
        ]);
    }

    public function store()
    {
        header("Content-Type: application/json");
        $request = json_decode(file_get_contents('php://input'), true);
        $input = $request['data']['input'];
        $categories = $input["categories"];
        unset($input['categories']);
        $input['slug'] = $input['title'];
        $product = new Product();
        if ($product->attach("product_categories", $input, $categories)) {
            header("HTTP/1.0 201");
        } else {
            header("HTTP/1.0 400");
        }
    }

    public function edit($params)
    {
        $product = (new Product())->find($params['id']);
        $categories = (new Product())->categories($product['id']);
        $allCategories = (new Category())->getAll();

        header("HTTP/1.0 200");
        View::render("products.edit", [
            "product" => $product,
            "categories" => $categories,
            "allCategories" => $allCategories
        ]);
    }

    public function update($params)
    {
        header("Content-Type: application/json");
        $request = json_decode(file_get_contents('php://input'), true);
        $input = $request['data']['input'];
        $categories = $input["categories"];
        unset($input['categories']);
        $input['slug'] = $input['title'];
        $input['id'] = $params['id'];
        $product = new Product();
        if ($product->update("product_categories", $input, $categories, $params['id'])) {
            header("HTTP/1.0 201");
        } else {
            header("HTTP/1.0 400");
        }
    }

    public function delete($params)
    {
        $products = new Product();
        $products->deleteRelation('product_categories', 'product_id', $params['id']);
        $products->delete('id', $params['id']);
        header("HTTP/1.0 204");
    }
}