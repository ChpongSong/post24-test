<?php


namespace App\Models;


class Category extends Model
{
    /**
     * @var string
     */
    protected string $table = "categories";

    /**
     * @param int $id
     * @return array
     */
    public function sub_categories(int $id): array
    {
       $db = $this->databaseService->openConnection();
       $res = $db->query("SELECT * FROM {$this->table} WHERE parent_id = {$id}");
       $this->databaseService->closeConnection();
       return $res->fetchAll();
    }

    /**
     * @param int $id
     * @return array
     */
    public function products(int $id): array
    {
        $db = $this->databaseService->openConnection();
        $res = $db->query(
            "
            SELECT prod.* FROM `product_categories` as prod_cat LEFT JOIN `products` as prod
            ON prod_cat.product_id=prod.id WHERE prod_cat.category_id={$id}
        "
        );
        $this->databaseService->closeConnection();
        return $res->fetchAll();
    }
}