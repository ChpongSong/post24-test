<?php


namespace App\Models;


use App\Helpers\SqlBuilder;

class Product extends Model
{

    const RELATIONS = [
        'product_categories' => [
            'type' => self::MANY_TO_MANY,
            'class' => ProductCategory::class
        ]
    ];

    /**
     * @var string
     */
    protected string $table = "products";

    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }


    /**
     * @param int $id
     * @return array
     */
    public function categories(int $id): array
    {
        $db = $this->databaseService->openConnection();
        $res = $db->query("
            SELECT cat.name, cat.slug, cat.id FROM `product_categories` as prod_cat LEFT JOIN `categories` as cat
            ON prod_cat.category_id=cat.id WHERE prod_cat.product_id={$id}
        ");
        $this->databaseService->closeConnection();
        return $res->fetchAll();
    }

    /**
     * @param $relation
     * @param array $data
     * @param $relData
     * @return int
     */
    public function attach($relation, array $data, $relData): int
    {
        $db = $this->databaseService->openConnection();
        if (array_key_exists($relation, self::RELATIONS)) {
            try {
                $db->beginTransaction();
                $db->prepare(SqlBuilder::generateInsertSql($this->table, $data))->execute($data);
                $localKey = $db->lastInsertId();
                foreach ($relData as $rel) {
                    $db->prepare("INSERT INTO `product_categories` (product_id, category_id) 
                                VALUES (?, ?)")->execute([$localKey, $rel]);
                }

                $db->commit();
            } catch (\Exception $exception) {
                dump($exception->getMessage());
                $db->rollBack();
            }

            return 1;
        }
        return 0;
    }

    public function update($relation, array $data, $relData, int $id)
    {
        $db = $this->databaseService->openConnection();
        if (array_key_exists($relation, self::RELATIONS)) {
            try {
                $db->beginTransaction();
                $res = $db->prepare("UPDATE $this->table SET title=:title, body=:body, slug=:slug, 
                 quantity=:quantity, price=:price WHERE id=:id");
                $res->execute($data);
                foreach ($relData as $rel) {
                    $db->prepare("INSERT INTO `product_categories` (product_id, category_id) 
                                VALUES (?, ?)")->execute([$id, $rel]);
                }

                $db->commit();
            } catch (\Exception $exception) {
                dump($exception->getMessage());
                $db->rollBack();
            }

            return 1;
        }
        return 0;
    }
}