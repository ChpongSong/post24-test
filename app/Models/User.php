<?php


namespace App\Models;

use PDOStatement;

class User extends Model
{
    protected string $table = "users";

    public function getByCredentials($input)
    {
        return $this->checkCredentials($input);
    }

    /**
     * @param $input
     * @return boolean|PDOStatement
     */
    private function checkCredentials($input)
    {
        $db = $this->databaseService->openConnection();
        $res = $db->prepare("SELECT * FROM {$this->table} WHERE email=:email
                      AND password=:password");
        $res->execute([
            ":email" => $input['email'],
            ":password" => md5($input['password'])
        ]);
        return $res->fetch();
    }
}