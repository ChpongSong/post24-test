<?php

namespace App\Models;

use App\Contracts\RelationTypes;
use App\Services\DatabaseService;

abstract class Model implements RelationTypes
{


    /**
     * @var DatabaseService
     */
    public DatabaseService $databaseService;
    /**
     * @var string
     */
    protected string $table = "";

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->databaseService = new DatabaseService();
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $db = $this->databaseService->openConnection();
        return $db->query("SELECT * FROM {$this->table}")->fetchAll();
    }

    public function find(int $id)
    {
        $db = $this->databaseService->openConnection();
        return $db->query("SELECT * FROM {$this->table} WHERE id={$id}")->fetch();
    }

    /**
     * @param string $relation
     * @param $key
     * @param $value
     * @return bool
     */
    public function deleteRelation(string $relation, $key, $value): bool
    {
        $db = $this->databaseService->openConnection();
        return $db->query("DELETE FROM `{$relation}` WHERE {$key}={$value}")->execute();
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    public function delete($key, $value): bool
    {
        $db = $this->databaseService->openConnection();
        return $db->query("DELETE FROM `{$this->table}` WHERE {$key}={$value}")->execute();
    }
}