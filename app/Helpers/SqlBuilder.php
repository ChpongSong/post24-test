<?php

namespace App\Helpers;

class SqlBuilder
{

    public static function generateInsertSql($tableName, $input): string
    {
        $res = self::generateTable($input);
        return "INSERT INTO {$tableName} {$res['tables']} VALUES {$res['values']}";
    }


    /**
     * @param $data
     * @return array
     */
    private static function generateTable($data): array
    {
        $tables = "(";
        $values = "(";
        $counter = 0;
        foreach ($data as $key => $value) {
            $tables .= $key . ', ';
            $values .= ':' . $key . ', ';
            $counter++;
        }
        $tables = substr($tables, 0, -2);
        $values = substr($values, 0, -2);
        $tables .= ")";
        $values .= ")";
        return [
            "tables" => $tables,
            "values" => $values,
            "counter" => $counter
        ];
    }
}