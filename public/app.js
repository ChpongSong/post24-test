const productSearch = document.getElementById("productSearch");
const productList = document.getElementById("product-list");
let timeout = null;
const userLogoutBtn = document.getElementById("userLogout") || null;

productSearch.addEventListener("focus", async function (event) {
    const res = await axios.get(`${window.location.origin}/api/getPopularProducts`);
    for (let product of res.data) {
        const option = document.createElement("option");
        option.value = product.title;
        option.setAttribute("data-product-link", `${window.location.origin}/products/show/${product.id}`)
        productList.appendChild(option);
    }
});

productSearch.addEventListener("focusout", function () {
    clearDataList();
});

function clearDataList() {
    productList.querySelectorAll("*").forEach(node => node.remove());
}

productSearch.addEventListener("keyup", function (event) {
    clearTimeout(timeout);
    timeout = setTimeout(async function (keyBoardEvent) {
        const inputValue = keyBoardEvent.target.value;
        const res = await axios.post(`${window.location.origin}/api/getProductByName`, {
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                productName: inputValue
            }
        })
        clearDataList();
        for (let product of await res.data) {
            const op = document.createElement("option")
            op.value = product.title;
            op.setAttribute("data-product-link", `${window.location.origin}/products/show/${product.id}`)
            productList.appendChild(op);
        }

    }, 1000, (event));
    if (event.keyCode === 13) {
        const value = event.target.value;
        let optionLink = "";
        productList.childNodes.forEach(item => {
            optionLink = item.dataset
        })
        window.location.replace(optionLink.productLink);
    }
});

if (userLogoutBtn !== null) {
    userLogoutBtn.addEventListener("click", async function () {
        const res = await axios.post(`${window.location.origin}/auth/logout`)
        console.log(res)
        if (res.status === 200) {
            window.location.replace(`${window.location.origin}/auth/login`);
        }
    });
}
