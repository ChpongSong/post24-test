<?php

require_once __DIR__ . "/../vendor/autoload.php";
require_once __DIR__ . "/../storage/bootstrap.php";

define("ROOT_PATH", getenv('ROOT_DIR'));
