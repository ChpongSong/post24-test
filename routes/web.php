<?php

return [
    "auth-group" => [
        "namespace" => "Auth",
        "prefix" => "auth",
        "endpoints" => [
            "login" => "AuthController@login#GET",
            "register" => "AuthController@register#GET",
            "signIn" => "AuthController@signIn#POST",
            "signUp" => "AuthController@signUp#POST",
            "logout" => "AuthController@logout#POST"
        ]
    ],
    "products-group" => [
        "namespace" => "Products",
        "prefix" => "products",
        "endpoints" => [
            "" => "ProductController@index#GET",
            "create" => "ProductController@create#GET",
            "store" => "ProductController@store#POST",
            "/show/:id" => "ProductController@show#GET",
            "/edit/:id" => "ProductController@edit#GET",
            "/update/:id" => "ProductController@update#POST",
            "/delete/:id" => "ProductController@delete#POST"
        ]
    ],
    "/api/getProductByName" => "API\\SearchController@getProductByName#POST",
    "/api/getPopularProducts" => "API\\SearchController@getPopularProducts#GET",
    "/" => "Products\\ProductController@index#GET"
];